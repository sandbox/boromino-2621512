<?php

/**
 * @file
 * Synchronize double field subfields separately.
 */

/**
 * Define prefix for form field name.
 */
define('DOUBLE_FIELD_I18N_SYNC_SUBFIELD_PREFIX', 'double_field_i18n_sync_');

/**
 * Implements hook_field_info_alter().
 */
function double_field_i18n_sync_field_info_alter(&$info) {
  if (isset($info['double_field'])) {
    $info['double_field']['i18n_sync_callback'] = 'double_field_i18n_sync_callback';
  }
}

/**
 * Translation synchronization callback.
 *
 * Synchronize double field module's subfields separately.
 *
 * @param string $entity_type
 *   Type of entity to synchronize.
 * @param object $entity
 *   Entity to update.
 * @param array $field
 *   Field definition.
 * @param array $instance
 *   Field instance definition.
 * @param string $langcode
 *   Language of entity to update.
 * @param array $items
 *   Field instance items with values to synchronize.
 * @param object $source_entity
 *   Entity from which to copy the values.
 * @param string $source_langcode
 *   Language of source entity.
 */
function double_field_i18n_sync_callback($entity_type, $entity, array $field, array $instance, $langcode, array &$items, $source_entity, $source_langcode) {
  $name = DOUBLE_FIELD_I18N_SYNC_SUBFIELD_PREFIX . $entity_type . '_' . $source_entity->type . '__' . $field['field_name'];
  $subfield = variable_get($name, 0);
  if (!$subfield) {
    return;
  }

  $source = (!empty($source_entity->original)) ? $source_entity->original : $source_entity;
  $source_items = field_get_items($entity_type, $source, $field['field_name'], $source_langcode);
  $destination_items = field_get_items($entity_type, $entity, $field['field_name'], $langcode);
  if (count($source_items) != count($destination_items)) {
    drupal_set_message(t('Different %field_name items quantity.', array('%field_name' => $field['field_name'])), 'error', TRUE);
    $items = $destination_items;
    return;
  }

  // Reverse copying because $items is copied to the entity translation in
  // i18n_sync_field_translation_sync().
  $subfield = ($subfield == 1) ? 'second' : 'first';
  foreach ($items as $delta => $item) {
    $items[$delta][$subfield] = $destination_items[$delta][$subfield];
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function double_field_i18n_sync_form_node_type_form_alter(&$form, &$form_state, $form_id) {
  if (!isset($form['i18n_sync']['i18n_sync_node_type'])) {
    return;
  }

  $node_type = $form['#node_type'];
  $records = double_field_i18n_sync_get_field_instances(
    array('double_field'),
    array('field_name'),
    array('node'),
    array($node_type->type),
    'field_name'
  );
  if (!count($records)) {
    return;
  }

  // Prepare element weight in order to add subfield form after corresponding
  // form field.
  $weight = 0;
  foreach ($form['i18n_sync']['i18n_sync_node_type'] as &$element) {
    $element['#weight'] = $weight;
    $weight += 10;
  }

  foreach ($records as $record) {
    if (isset($form['i18n_sync']['i18n_sync_node_type'][$record->field_name])) {
      $name = DOUBLE_FIELD_I18N_SYNC_SUBFIELD_PREFIX . 'node_' . $node_type->type . '__' . $record->field_name;
      $form['i18n_sync']['i18n_sync_node_type'][$name] = array(
        '#type' => 'select',
        '#title' => t('Subfields'),
        '#options' => array(t('Both'), 'first', 'second'),
        '#default_value' => variable_get($name, 0),
        '#description' => t('Choose what subfield(s) to synchronize.'),
        '#weight' => ($form['i18n_sync']['i18n_sync_node_type'][$record->field_name]['#weight'] + 1),
        '#states' => array(
          'visible' => array(
            ':input[name="i18n_sync_node_type[' . $record->field_name . ']"]' => array('checked' => TRUE),
          ),
        ),
      );
    }
  }

  $form['#submit'][] = 'double_field_i18n_sync_node_type_form_submit';
}

/**
 * Submit handler for node type double field i18n sync form.
 *
 * @see double_field_i18n_sync_form_node_type_form_alter()
 *
 * @todo Delete variable if node type machine name changes.
 */
function double_field_i18n_sync_node_type_form_submit($form, &$form_state) {
  $node_type = $form['#node_type'];
  $records = double_field_i18n_sync_get_field_instances(
    array('double_field'),
    array('field_name'),
    array('node'),
    array($node_type->type),
    'field_name'
  );

  foreach ($records as $record) {
    $name = DOUBLE_FIELD_I18N_SYNC_SUBFIELD_PREFIX . 'node_' . $node_type->type . '__' . $record->field_name;
    if (isset($form_state['values']['i18n_sync_node_type'][$name])) {
      variable_set($name, $form_state['values']['i18n_sync_node_type'][$name]);
    }
  }
}

/**
 * Return field instance configuration.
 *
 * @param array $field_type
 *   The type of field(s) to query for.
 * @param array $fields
 *   Database table column names to return in the query results.
 * @param array $entity_type
 *   Entity type the field(s) belong to.
 * @param string $group_by
 *   Database table column name to group by (DISTINCT).
 *
 * @return array
 *   Field instance configuration.
 */
function double_field_i18n_sync_get_field_instances(array $field_type, array $fields, array $entity_type, array $bundle = array(), $group_by = NULL) {
  $query = db_select('field_config_instance', 'fci');
  $query->join('field_config', 'fc', 'fci.field_id = fc.id');
  $query->fields('fci', $fields);
  $query->condition('fci.entity_type', $entity_type, 'IN');
  $query->condition('fc.type', $field_type, 'IN');

  if (count($bundle)) {
    $query->condition('fci.bundle', $bundle, 'IN');
  }

  if (!empty($group_by)) {
    $query->groupBy($group_by);
  }

  return $query->execute()->fetchAll();
}
